import src.pso.pso as pso
import src.pso.cost_functions as cost_functions


def test_pso():
    initial_point = [5, 5]  # initial starting location [x1,x2...]
    bounds = [(-10, 10), (-10, 10)]  # input bounds [(x1_min,x1_max),(x2_min,x2_max)...]
    # we have 2D coordinates: x and y
    coordinates_count = 2
    pso.PSO(cost_functions.cost_function1, initial_point, bounds,
            particles_count=15, max_iterations=30, coordinates_count=coordinates_count)