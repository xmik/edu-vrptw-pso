import src.vrp.customer as customer


def test_customer_distance():
    c1 = customer.Customer(1,2, 0,0,0,0)
    c2 = customer.Customer(5,5, 0,0,0,0)
    distance = c1.compute_distance_to(c2)
    assert distance == 5