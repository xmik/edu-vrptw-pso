import src.pso.particle as particle

class PSO():
    def __init__(self, costFunc, initial_point, bounds, particles_count, max_iterations, coordinates_count):
        smallest_error_for_group=-1
        best_position_for_group=[]

        # create a swarm of particles
        swarm=[]
        for i in range(0, particles_count):
            swarm.append(particle.Particle(initial_point, coordinates_count))

        # begin optimization loop
        iteration = 1
        while iteration < max_iterations:
            # cycle through particles in swarm and evaluate fitness
            for j in range(0, particles_count):
                swarm[j].evaluate(costFunc)

                # determine if current particle is the best (globally)
                if swarm[j].error < smallest_error_for_group or smallest_error_for_group == -1:
                    best_position_for_group=list(swarm[j].position)
                    smallest_error_for_group=float(swarm[j].error)

            # cycle through swarm and update velocities and position
            for j in range(0, particles_count):
                swarm[j].update_velocity(best_position_for_group)
                swarm[j].update_position(bounds)
            iteration += 1

        # print final results
        print('Summary:')
        print('Iterations run: ' + str(iteration))
        print('Best particle position: ' + str(best_position_for_group))
        print('Best particle error: ' + str(smallest_error_for_group))