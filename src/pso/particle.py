import random

class Particle:
    def __init__(self, initial_point, coordinates_count):
        self.position=[]
        self.velocity=[]
        self.error=-1
        self.best_position=[]
        self.best_position_error=-1
        self.coordinates_count = coordinates_count

        # initialize particles to have
        # * random velocity
        # * the same position as the initial_point
        for i in range(0, coordinates_count):
            self.velocity.append(random.uniform(-1,1))
            self.position.append(initial_point[i])


    def __str__(self):
        print("Particle at: ", self.position, ", best position: ", self.best_position)


    # evaluate the particle using a fitness function
    def evaluate(self, cost_func):
        self.error = cost_func(self.position)

        # check to see if the current position is an individual best
        if self.error < self.best_position_error or self.best_position_error == -1:
            self.best_position = self.position
            self.best_position_error = self.error


    # update new particle velocity
    def update_velocity(self,pos_best_g):
        w=0.5       # constant inertia weight (how much to weigh the previous velocity)
        c1=1        # cognitive constant
        c2=2        # social constant

        for i in range(0, self.coordinates_count):
            r1=random.random()
            r2=random.random()

            vel_cognitive=c1*r1*(self.best_position[i]-self.position[i])
            vel_social=c2*r2*(pos_best_g[i]-self.position[i])
            self.velocity[i]=w*self.velocity[i]+vel_cognitive+vel_social


    # update the particle position based on new velocity updates
    # (move the particle)
    def update_position(self,bounds):
        for i in range(0, self.coordinates_count):
            self.position[i]=self.position[i]+self.velocity[i]

            # adjust maximum position if necessary
            if self.position[i]>bounds[i][1]:
                self.position[i]=bounds[i][1]

            # adjust minimum position if necessary
            if self.position[i] < bounds[i][0]:
                self.position[i]=bounds[i][0]

# class Particle():
#     def __init__(self):
#         self.position = np.array([(-1) ** (bool(random.getrandbits(1))) * random.random()*50, (-1)**(bool(random.getrandbits(1))) * random.random()*50])
#         self.pbest_position = self.position
#         self.pbest_value = float('inf')
#         self.velocity = np.array([0,0])
#
#     def move(self):
#         self.position = self.position + self.velocity
