class Solution:
    def __init__(self, routes_collection, how_important_is_minimizing_vehicles_count=0.9):
        self.routes_collection = routes_collection
        self.vehicles_count = len(routes_collection)
        if how_important_is_minimizing_vehicles_count > 1.0:
            raise ValueError('how_important_is_minimizing_vehicles_count was > 1.0: ' + how_important_is_minimizing_vehicles_count)
        self.how_important_is_minimizing_vehicles_count = how_important_is_minimizing_vehicles_count
        self.how_important_is_minimizing_routes_length = 1.0 - how_important_is_minimizing_vehicles_count

    def compute_routes_length(self):
        raise ValueError('TODO: not implemented')



