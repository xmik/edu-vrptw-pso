import src.vrp.solution_provider as solution_provider

# this function combines 2 objective functions:
# 1. to minimize the number of vehicles
# 2. to minimize the total routes length
#
# we can customize which objective function is more important by setting
# the how_important_is_minimizing_vehicles_count parameter
def compute_fitness(particle_position):
    solution = solution_provider.get_solution_from_particle_position(particle_position)
    return solution.how_important_is_minimizing_vehicles_count * solution.vehicles_count + \
           solution.how_important_is_minimizing_routes_length * solution.compute_routes_length