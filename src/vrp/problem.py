class Problem:
    def __init__(self, name, vehicles_count, vehicles_capacity, customers):
        self.name = name
        self.vehicles_count = vehicles_count
        self.vehicles_capacity = vehicles_capacity
        self.customers = list(customers)