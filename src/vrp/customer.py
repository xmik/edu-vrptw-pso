import math

class Customer:
    def __init__(self, x_coord, y_coord, demand, ready_time, due_time, service_time):
        self.x_coord = x_coord
        self.y_coord = y_coord
        self.demand = demand
        self.ready_time = ready_time
        self.due_time = due_time
        self.service_time = service_time

    def compute_distance_to(self, other_customer):
        return math.sqrt(
                    math.pow((self.x_coord - other_customer.x_coord), 2) + \
                    math.pow((self.y_coord - other_customer.y_coord), 2)
                )
