import itertools
import src.vrp.solution as solution_lib

# get one particular solution based on the particle position
def get_solution_from_particle_position(particle_position):
    raise ValueError('TODO: not implemented')

    my_solution = solution_lib.Solution()
    return my_solution


# enumerate all the possible routes
#
# e.g. we have 3 customers (+ 1 depot), so: max_vehicles_count = 3 and min_vehicles_count = 1.
# All the possible solutions are:
# S1 = ((c1, c2, c3)) # all customers visited by one vehicle, does not matter which vehicle
# S2 = ((c1, c2), (c3)) # c1 and c2 visited by 1 vehicle, c3 visited by the second vehicle
# S3 = ((c1, c3), (c2)) # c1 and c3 visited by 1 vehicle, c2 visited by the second vehicle
# S4 = ((c2, c3), (c1)) # c2 and c3 visited by 1 vehicle, c1 visited by the second vehicle
# S5 = ((c1), (c2), (c3)) # each customer visited by 1 vehicle
#
# * The order here matters, because even if: (c1, c2, c3) should be the same as (c3, c2, c1), it is not
# the same as (c2, c1, c3).
# * We don't want repetitions (each customer must be visited just once).
# * we want sth like variations without repetitions?
def get_all_solutions(customers_count):
    raise ValueError('TODO: not implemented')

    # do not count the depot
    customers_count_without_depot = customers_count - 1
    max_vehicles_count = customers_count_without_depot
    min_vehicles_count = 1