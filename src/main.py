import src.pso.pso as pso
import src.vrp.fitness_function as fitness_function
import src.test_data as test_data
import src.vrp.solution_provider as solution_provider


def main():
    p = test_data.get_test_problem_simple()
    solutions = solution_provider.get_all_solutions(len(p.customers))

    initial_point = p.customers[0]
    bounds = [(0), ( len(solutions) )]
    # we have 1D coordinates: number of solutions
    coordinates_count = 1
    pso.PSO(fitness_function.compute_fitness, initial_point, bounds,
            particles_count=15, max_iterations=30, coordinates_count=coordinates_count)

if __name__ == "__main__":
    main()


