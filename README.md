# VRPTW solved by PSO

This is an academic project.

The code is heavily inspired on: https://nathanrooy.github.io/posts/2016-08-17/simple-particle-swarm-optimization-with-python/

## Usage

Run the project with:
```
./tasks _run
```

Test the project with:
```
./tasks _test
```

## Links

   * https://github.com/iRB-Lab/py-ga-VRPTW/
   * https://deap.readthedocs.io/en/master/examples/pso_basic.html
   * https://nathanrooy.github.io/posts/2016-08-17/simple-particle-swarm-optimization-with-python/ and https://nathanrooy.github.io/posts/2016-08-17/simple-particle-swarm-optimization-with-python/